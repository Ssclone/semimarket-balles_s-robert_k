package epimarket.semimarket;

import java.sql.Connection;
import java.sql.SQLException;

import epimarket.semimarket.dao.CategoryDAO;
import epimarket.semimarket.dao.ClientDAO;
import epimarket.semimarket.dao.DAO;
import epimarket.semimarket.dao.OrderDAO;
import epimarket.semimarket.dao.ProductDAO;
import epimarket.semimarket.db.DbManager;
import epimarket.semimarket.model.Category;
import epimarket.semimarket.model.Client;
import epimarket.semimarket.model.Order;
import epimarket.semimarket.model.Product;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException
    {
    	DbManager dbM = new DbManager();
    	Connection conn = dbM.getDatasource().getConnection();
    	DAO<Product> daoProd = new ProductDAO(conn);
    	
    	DAO<Client> daoClt = new ClientDAO(conn);
    	DAO<Category> daoCat = new CategoryDAO(conn);
    	DAO<Order> daoOrd = new OrderDAO(conn);
    	
//    	Product prod = daoProd.find(8);
//    	System.out.println(prod.getId() + ": produit=" + prod.getName() + ", price=" + prod.getPrice() + ", categorie=" + prod.getCategory());
//    	prod.setCategory(6);
//    	prod.setPrice(200);
//    	daoProd.update(prod);
//    	
//    	daoProd.delete(prod);
    	
//    	Product prod = new Product("Pet", 100, 3);
    	Client clt = new Client("Jean", "Philippe");
    	Category cat = new Category("Chat");
    	Order order = new Order(clt.getId(), cat.getId());

//    	daoProd.create(prod);
    	daoClt.create(clt);
    	daoCat.create(cat);
    	daoOrd.create(order);
    	
    	
//    	System.out.println("Notre pet a pour ID : " + prod.getId());
    	System.out.println(clt.getfirstName() + " " + clt.getlastName() + " a pour id : " + clt.getId());
    	System.out.println("Notre categorie " + cat.getName() + " a pour ID : " + cat.getId());
    	System.out.println("Notre commande a pour ID : " + order.getId());
    	
//    	Product prod = daoProd.find(1);    	
//    	System.out.println("Notre Produit de la mort qui tue est un " + prod.getName());
//    	Product prod2 = daoProd.find(2);
//    	System.out.println("Notre Produit de la mort qui tue est un " + prod2.getName());

    }
}