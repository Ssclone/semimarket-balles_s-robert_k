package epimarket.semimarket.db;



import javax.sql.DataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;


public class DbManager {

	private DataSource datasource;
	
	public DbManager()
	{
		MysqlDataSource ds = new MysqlDataSource();
		ds.setUser("root");
		ds.setPassword("root");
		ds.setServerName("localhost");
		ds.setDatabaseName("Epimarket");
		this.datasource = ds;
	}

	public DataSource getDatasource() {
		return datasource;
	}
		
}
