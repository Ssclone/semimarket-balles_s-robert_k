package epimarket.semimarket.model;


public class Order {
	
	private int id = 0;
		
	private int client_id = 0;
	
	private int product_id = 0;
	
	
	public Order(int client_id, int product_id)
	{
		this.client_id = client_id;
		this.product_id = product_id;
	}
	
	public Order() {}

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClient_id() {
		return client_id;
	}

	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}


}
