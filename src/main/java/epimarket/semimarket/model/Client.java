package epimarket.semimarket.model;

public class Client {
	
	private int id = 0;
	
	private String lastName = "";
	
	private String firstName = "";
	
	public Client(String firstName, String lastName)
	{
		this.lastName = lastName;
		this.firstName = firstName;
	}
	
	public Client() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getlastName() {
		return lastName;
	}

	public void setlastName(String lastName) {
		this.lastName = lastName;
	}

	public String getfirstName() {
		return firstName;
	}

	public void setfirstName(String firstName) {
		this.firstName = firstName;
	};
	
	

}
