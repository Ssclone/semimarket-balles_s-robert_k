package epimarket.semimarket.model;


public class Product {
	
	private int id = 0;
	
	private String name = "";
	
	private int price = 0;
	
	private int category_id = 0;
		
	public Product(String name, int price, int category_id)
	{
		this.name = name;
		this.price = price;
		this.category_id = category_id;
	}
	
	public Product() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getCategory() {
		return category_id;
	}

	public void setCategory(int category) {
		this.category_id = category;
	}


}
