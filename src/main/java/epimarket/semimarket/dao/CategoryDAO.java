package epimarket.semimarket.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import epimarket.semimarket.model.Category;

public class CategoryDAO extends DAO<Category>{
	public CategoryDAO(Connection conn)
	{
		super(conn);
	}

	@Override
	public boolean create(Category obj) {
		
		int res = 0;
		boolean ret = false;
		ResultSet rs = null;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("INSERT INTO Category (name) VALUES(?)");
			stmt.setString(1, obj.getName());
		
			res = stmt.executeUpdate();
			DbUtils.closeQuietly(stmt);
			stmt = null;

			ret = res > 0 ? true : false;
			
			stmt = this.connect.prepareStatement("SELECT id FROM Category ORDER BY id DESC LIMIT 1");
			
			rs = stmt.executeQuery();
			rs.first();
			
			obj.setId(rs.getInt("id"));
			
			if (ret) {
				System.out.println("Categorie ajoutée.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
    	}
		return ret;
	}

	@Override
	public boolean delete(Category obj) {
		int res = 0;
		boolean ret = false;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("DELETE from Category where id=?");
			stmt.setInt(1, obj.getId());
		
			res = stmt.executeUpdate();
								
			ret = res > 0 ? true : false;
			
			if (ret) {
				System.out.println("Categorie retiré.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
    	}
		return ret;
	}

	@Override
	public boolean update(Category obj) {
		
		int res = 0;
		boolean ret = false;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("UPDATE Category set name=? where id=?");
			stmt.setString(1, obj.getName());
			stmt.setInt(2, obj.getId());
		
			res = stmt.executeUpdate();
								
			ret = res > 0 ? true : false;
			
			if (ret) {
				System.out.println("Categorie mise à jour.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
    	}
		return ret;
	}

	@Override
	public Category find(int id) {

		ResultSet rs = null;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("SELECT * FROM Category where id=?");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
						
			rs.first();
			Category prod = new Category(rs.getString("name"));
			prod.setId(id);
			
			return prod;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
    		DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
    	}
		return null;
    }
	
}
