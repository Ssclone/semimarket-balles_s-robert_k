package epimarket.semimarket.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import epimarket.semimarket.model.Product;

public class ProductDAO extends DAO<Product>{
	public ProductDAO(Connection conn)
	{
		super(conn);
	}

	@Override
	public boolean create(Product obj) {
		
		int res = 0;
		boolean ret = false;
		ResultSet rs = null;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("INSERT INTO Product (name, price, category_id) VALUES(?, ?, ?)");
			stmt.setString(1, obj.getName());
			stmt.setInt(2, obj.getPrice());
			stmt.setInt(3, obj.getCategory());
		
			res = stmt.executeUpdate();
			DbUtils.closeQuietly(stmt);
			stmt = null;

			ret = res > 0 ? true : false;
			
			stmt = this.connect.prepareStatement("SELECT id FROM Product ORDER BY id DESC LIMIT 1");
			
			rs = stmt.executeQuery();
			rs.first();
			
			obj.setId(rs.getInt("id"));
			
			if (ret) {
				System.out.println("Produit ajouté.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
    	}
		return ret;
	}

	@Override
	public boolean delete(Product obj) {
		int res = 0;
		boolean ret = false;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("DELETE from Product where id=?");
			stmt.setInt(1, obj.getId());
		
			res = stmt.executeUpdate();
								
			ret = res > 0 ? true : false;
			
			if (ret) {
				System.out.println("Produit retiré.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}	
    	finally {
			DbUtils.closeQuietly(stmt);
    	}
		return ret;
	}

	@Override
	public boolean update(Product obj) {
		
		int res = 0;
		boolean ret = false;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("UPDATE Product set name=?, price=?, category_id=? where id=?");
			stmt.setString(1, obj.getName());
			stmt.setInt(2, obj.getPrice());
			stmt.setInt(3, obj.getCategory());
			stmt.setInt(4, obj.getId());
		
			res = stmt.executeUpdate();
								
			ret = res > 0 ? true : false;
			
			if (ret) {
				System.out.println("Produit mis à jour.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
    	}
		return ret;
	}

	@Override
	public Product find(int id) {

		ResultSet rs = null;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("SELECT * FROM Product where id=?");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
						
			rs.first();
			Product prod = new Product(rs.getString("name"), rs.getInt("price"), rs.getInt("category_id"));
			prod.setId(id);
			
			return prod;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
    		DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
    	}
		return null;
    }
	
}
