package epimarket.semimarket.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import epimarket.semimarket.model.Order;

public class OrderDAO extends DAO<Order>{
	public OrderDAO(Connection conn)
	{
		super(conn);
	}

	@Override
	public boolean create(Order obj) {
		
		int res = 0;
		boolean ret = false;
		ResultSet rs = null;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("INSERT INTO OrderLine (client_id, product_id) VALUES(?, ?)");
			stmt.setInt(1, obj.getClient_id());
			stmt.setInt(2, obj.getProduct_id());
		
			res = stmt.executeUpdate();
			DbUtils.closeQuietly(stmt);
			stmt = null;

			ret = res > 0 ? true : false;
			
			stmt = this.connect.prepareStatement("SELECT id FROM OrderLine ORDER BY id DESC LIMIT 1");
			
			rs = stmt.executeQuery();
			rs.first();
			
			obj.setId(rs.getInt("id"));
			
			if (ret) {
				System.out.println("Commande ajoutée.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
    	}
		return ret;
	}

	@Override
	public boolean delete(Order obj) {
		int res = 0;
		boolean ret = false;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("DELETE from OrderLine where id=?");
			stmt.setInt(1, obj.getId());
		
			res = stmt.executeUpdate();
								
			ret = res > 0 ? true : false;
			
			if (ret) {
				System.out.println("Commande retirée.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
    	}
		return ret;
	}

	@Override
	public boolean update(Order obj) {
		
		int res = 0;
		boolean ret = false;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("UPDATE OrderLine set client_id=?, product_id=? where id=?");
			stmt.setInt(1, obj.getClient_id());
			stmt.setInt(2, obj.getProduct_id());
			stmt.setInt(3, obj.getId());
		
			res = stmt.executeUpdate();
								
			ret = res > 0 ? true : false;
			
			if (ret) {
				System.out.println("Commande mise à jour.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
    	}
		return ret;
	}

	@Override
	public Order find(int id) {

		ResultSet rs = null;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("SELECT * FROM OrderLine where id=?");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
						
			rs.first();
			Order prod = new Order(rs.getInt("client_id"), rs.getInt("product_id"));
			prod.setId(id);
			
			return prod;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
    		DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
    	}
		return null;
    }
	
}
