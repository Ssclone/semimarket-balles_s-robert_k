package epimarket.semimarket.dao;

import java.sql.Connection;


public abstract class DAO<T> {
	protected Connection connect = null;

	public DAO(Connection conn) {
		this.connect = conn;
	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public abstract boolean create(T obj);
	
	/**
	 * 
	 * @param obj
	 * @return
	 */
	public abstract boolean delete(T obj);
	
	/**
	 * 
	 * @param obj
	 * @return
	 */
	public abstract boolean update(T obj);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public abstract T find(int id);
}
