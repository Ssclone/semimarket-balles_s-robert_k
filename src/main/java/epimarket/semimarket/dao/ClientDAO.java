package epimarket.semimarket.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import epimarket.semimarket.model.Client;

public class ClientDAO extends DAO<Client>{
	public ClientDAO(Connection conn)
	{
		super(conn);
	}

	@Override
	public boolean create(Client obj) {
		
		int res = 0;
		boolean ret = false;
		ResultSet rs = null;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("INSERT INTO Client (fst_name, lst_name) VALUES(?, ?)");
			stmt.setString(1, obj.getfirstName());
			stmt.setString(2, obj.getlastName());
		
			res = stmt.executeUpdate();
			DbUtils.closeQuietly(stmt);
			stmt = null;

			ret = res > 0 ? true : false;
			
			stmt = this.connect.prepareStatement("SELECT id FROM Client ORDER BY id DESC LIMIT 1");
			
			rs = stmt.executeQuery();
			rs.first();
			
			obj.setId(rs.getInt("id"));
			
			if (ret) {
				System.out.println("Client ajouté.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(rs);
    	}
		return ret;
	}

	@Override
	public boolean delete(Client obj) {
		int res = 0;
		boolean ret = false;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("DELETE from Client where id=?");
			stmt.setInt(1, obj.getId());
		
			res = stmt.executeUpdate();
								
			ret = res > 0 ? true : false;
			
			if (ret) {
				System.out.println("Client retiré.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
    	}
		return ret;
	}

	@Override
	public boolean update(Client obj) {
		
		int res = 0;
		boolean ret = false;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("UPDATE Client set fst_name=?, lst_name=? where id=?");
			stmt.setString(1, obj.getfirstName());
			stmt.setString(2, obj.getlastName());
			stmt.setInt(3, obj.getId());
		
			res = stmt.executeUpdate();
								
			ret = res > 0 ? true : false;
			
			if (ret) {
				System.out.println("Client mis à jour.");
			}
			else {
				System.out.println("Aucune opération effectuée.");
			}
			
			return ret;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
			DbUtils.closeQuietly(stmt);
    	}
		return ret;
	}

	@Override
	public Client find(int id) {

		ResultSet rs = null;
		PreparedStatement stmt = null;
    	
    	try
    	{
    		stmt = this.connect.prepareStatement("SELECT * FROM Client where id=?");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
						
			rs.first();
			Client prod = new Client(rs.getString("fst_name"), rs.getString("lst_name"));
			prod.setId(id);
			
			return prod;
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    	finally {
    		DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(stmt);
    	}
		return null;
    }
	
}
